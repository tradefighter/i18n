# Internationalisation (i18n) for Trade Fighter

This project contains resources used by
[Trade Fighter](https://tradefighter.io/)
to translate it into various languages.

This README file explains how to add support for new languages,
make changes for existing languages and how to make these changes
available in the game.

# Text translations

Text translations are in the [translations/](translations/) folder.
There is one
[JSON](https://en.wikipedia.org/wiki/JSON)
file per language, named for the
[ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)
language code. For example, the Japanese translation file is named
[ja.json](translations/ja.json).

## Translation file format

Each file contains many key/value pairs like:

```json
    "BvB": "ブル vs ベア"
```

Here we have:
  * the key `"BvB"` which the game uses to refer to this piece of text
  * the value `"ブル vs ベア"` which will appear on-screen when the user
  is playing in Japanese.

Similarly, the Korean translation file [kr.json](translations/kr.json)
contains `"BvB": "소 vs 곰"`; the same key `"BvB"` will appear on-screen
with the Korean translation for people playing in Korean language.

If you are not familiar with the format, here is a
[JSON quick guide](https://www.tutorialspoint.com/json/json_quick_guide.htm).

### Groups of keys

The translation keys are grouped according to how or where they are used
in the game. These groups include:

  * `common`: for words or short phrases that are used throughout the game,
  such as "Bull", "Bear", "Trade", "Fight" etc.
  * `links`: for links to external web sites, so that users can be directed
  to appropriate social media and documentation in their own language
  * `menu`, `footer`, `faq`, `buy`, `trade`, `fight`, `payout` etc.:
  Text for various screens and parts of screens in the game.

These are all under the top-level `"translation"` group.

## Creating a new translation file

Copy the [en.json](translations/en.json) file to a new file named for the
language code; for example if we were to create a new translation for Zulu
we would name the file `zu.json`.

## Editing a translation

Do not modify any of the key names, including the top-level key `"translations"`
and all the names of groups like `"links"`, `"common"` etc; modify only the
values. Considering, for example:

```json
        "faq": {
            "question1": "What is Trade Fighter?",
```
Do not modify the keys `faq` or `question1`, but do translate the text
`"What is Trade Fighter?"` into your language.

## Submitting your changes

All changes will go through a git merge request, after which they can be
pulled into the live game. This can be done entirely through GitLab's
[Web IDE](https://gitlab.com/-/ide/project/tradefighter/i18n/edit/master/-/translations/).

Please discuss with the developers on our Telegram or Discord channels
for assistance with preparing a merge request, or just to let us know
about your great work!

# Image translations

Coming soon!
